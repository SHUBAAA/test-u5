// Neuron.java
// From Classic Computer Science Problems in Java Chapter 7
// Copyright 2020 David Kopec
//
//
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


package ufro.model;

import ufro.util.Util;

import java.util.function.DoubleUnaryOperator;

public class Neuron {
	public final double[] weights;
	public final double learningRate;
	protected double outputCache;
	protected double delta;
	public final DoubleUnaryOperator activationFunction;
	public final DoubleUnaryOperator derivativeActivationFunction;

	public Neuron(double[] weights, double learningRate, DoubleUnaryOperator activationFunction,
			DoubleUnaryOperator derivativeActivationFunction) {
		this.weights = weights;
		this.learningRate = learningRate;
		outputCache = 0.0;
		delta = 0.0;
		this.activationFunction = activationFunction;
		this.derivativeActivationFunction = derivativeActivationFunction;
	}

	public double output(double[] inputs) {
		outputCache = Util.dotProduct(inputs, weights);
		return activationFunction.applyAsDouble(outputCache);
	}

}
