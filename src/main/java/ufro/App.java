package ufro;


import ufro.model.Network;
import ufro.util.Util;

import java.util.logging.Level;
import java.util.logging.Logger;

public class App {

    public static void main(String[] args) throws Util.ExceptionCustom {

        Logger logger = Logger.getLogger("ufro.App");
        Wine wineTest = new Wine();
        Network<Integer>.Results results = wineTest.classify();
        String mensaje = results.correct + " correct of " + results.trials + " = " +
                results.percentage * 100 + "%";
        logger.log(Level.FINE, mensaje);

    }
}
